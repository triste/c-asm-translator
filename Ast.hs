module Ast (Expr(..), BinOp(..), Def(..), Decl(..), Type(..), Stmt(..)) where

data BinOp = Assign
           | Add
           | Sub
           | Mul
           | Div
           deriving Show

data Expr = Const Int
          | Ident String
          | Bin BinOp Expr Expr
          | Block [Expr]
          deriving Show

data Type = Int
          | Float
          deriving Show

data Def = Def String (Maybe Expr)
         deriving Show

data Decl = Decl Type Def
          | DeclBlock [Decl]
          deriving Show

data Stmt = StmtBlock [Stmt]
          | StmtExpr Expr
          | StmtDecl Decl
          deriving Show
{-
data Decl = Decl Type Def
          | DeclBlock Type [Def]
          deriving Show
-}
