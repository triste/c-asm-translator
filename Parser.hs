{-# LANGUAGE Rank2Types #-}
module Parser (parse) where

import qualified Lex as L
import qualified Ast as A
import qualified Error as E

import Control.Applicative (Alternative(..))
import Control.Monad (ap)
import Data.Monoid ((<>))

type Input = [L.TokPos]

newtype Parser a = Parser {
        runP :: forall b. Input -> (a -> Input -> E.Error -> b) -> (E.Error -> b) -> b
}

instance Functor Parser where
        fmap f (Parser p) = Parser $ \x g b -> p x (\a i -> g (f a) i) b

instance Applicative Parser where
        pure = return
        (<*>) = ap

instance Monad Parser where
        return x = Parser $ \xs g _ -> g x xs mempty
        (>>=) (Parser p) f = Parser $ \x g b ->
                let ok a xs e = runP (f a) xs (\a xs e' -> g a xs (e <> e')) (b <$> (e <>))
                    in p x ok b

instance Alternative Parser where
        empty = undefined
        (<|>) (Parser l) (Parser r) = Parser $ \x g b ->
                l x g (\e -> r x (\a xs e' -> g a xs (e <> e')) (b <$> (e <>)))

{- Expressions
- Expr -> I | Expr , I
- I -> id = (I | E)
- E -> E + T | E - T | T
- T -> T * F | T / F | F
- F -> id | num | ( E )
-
- Declarations
- D -> Type Idl ;
- Ty -> int | float
- Idl -> Id | Idl , Id
- Id -> id | id = E
-
- Statements
- S -> { C } | Expr ;
- C -> C' C
- C' -> D | S
-}

errP :: String -> Parser a
errP msg = Parser $ \((x, c, l):_) _ b -> b (E.Expected msg (show x) c l)


-- Expressions
number :: Parser A.Expr
number = Parser $ \((x, c, l):xs) g b -> case x of L.Const n -> g (A.Const n) xs mempty
                                                   _ -> b (E.Expected "number" (show x) c l)

ident :: Parser A.Expr
ident = Parser $ \((x, c, l):xs) g b -> case x of L.Ident i -> g (A.Ident i) xs mempty
                                                  _ -> b (E.Expected "ident" (show x) c l)

term :: L.Token -> Parser ()
term tok = Parser $ \((x, c, l):xs) g b -> if x == tok then g () xs mempty else b (E.Expected (show tok) (show x) c l)

primExpr :: Parser A.Expr
primExpr = ident <|> number <|> (term L.Lpar >> expr <* term L.Rpar)

multExpr :: Parser A.Expr
multExpr = primExpr >>= multExpr'

multExpr' :: A.Expr -> Parser A.Expr
multExpr' x = (term L.Mul >> go A.Mul) <|> (term L.Div >> go A.Div) <|> return x
        where go t = primExpr >>= multExpr' . A.Bin t x {- FT' -}

additExpr :: Parser A.Expr
additExpr = multExpr >>= additExpr'

additExpr' :: A.Expr -> Parser A.Expr
additExpr' x = (term L.Add >> go A.Add) <|> (term L.Sub >> go A.Sub) <|> return x
        where go ch = multExpr >>= additExpr' . A.Bin ch x {- TE' -}

assigExpr :: Parser A.Expr
assigExpr = additExpr >>= \x -> do
        term L.Assign
        case x of
             A.Ident _ -> A.Bin A.Assign x <$> assigExpr
             _         -> errP "lvalue"
        <|> return x

expr :: Parser A.Expr
expr = assigExpr >>= \x -> A.Block . (x:) <$> some (term L.Comma >> assigExpr) <|> return x

-- Declarations
parseType :: Parser A.Type
parseType = Parser $ \((x, c, l):xs) g b -> case x of
                                                 L.KwInt -> g A.Int xs mempty
                                                 L.KwFloat -> g A.Float xs mempty
                                                 _ -> b (E.Expected "type" (show x) c l)

decl :: Parser A.Decl
decl = (parseType >>= initDeclList) <* term L.Semi -- FIX: (<*) doesn't return error

initDeclList :: A.Type -> Parser A.Decl
initDeclList t = initDecl >>= \x ->
        A.DeclBlock . (A.Decl t x:) <$> some (term L.Comma >> initDeclList t)
        <|> return (A.Decl t x)

initDecl :: Parser A.Def
initDecl = ident >>= \(A.Ident x) -> do
        term L.Assign
        e <- additExpr
        return $ A.Def x (Just e)
        <|> return (A.Def x Nothing)

-- Statements
stmt :: Parser A.Stmt
stmt = compStmt <|> (A.StmtExpr <$> expr <* term L.Semi)

compStmt :: Parser A.Stmt
compStmt = term L.Lcur >> A.StmtBlock <$> some (A.StmtDecl <$> decl <|> stmt) <* term L.Rcur

-- Global parse function
data Result a = Ok a Input E.Error
              | Err E.Error
              deriving Show

parse :: Input -> Either E.Error A.Expr
parse x = case runP expr x Ok Err of
               Ok ast ((L.Eof,_,_):_) _ -> Right ast
               Ok _ _ e -> Left e
               Err e -> Left e
