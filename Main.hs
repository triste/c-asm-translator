import Lex
import Parser
--import Optimizer
import Gen

main :: IO ()
main = do
        input <- getContents
        case parse (tokenize input) of
             Right ast -> generate $ optimize ast
             Left err -> print err
        where optimize = id -- TODO
