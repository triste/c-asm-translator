module Error (Error(..)) where

import Data.Monoid

data Error = Expected String String Int Int

instance Show Error where
        show (Expected x before c l) = show c ++ ":" ++ show l ++ " Expected ‘" ++ x ++ "’ before " ++ before

instance Semigroup Error where
        (<>) (Expected e b c l) (Expected e' b' c' l') = Expected (e ++ ", " ++ e') (b ++ ", " ++ b') c l


instance Monoid Error where
        mempty = Expected "0" "0" 0 0
        mappend (Expected e b c l) (Expected e' b' c' l') = Expected (e ++ ", " ++ e') (b ++ ", " ++ b') c l
{-
instance Monoid Error where
        mempty = Expected "" "" 0 0
        mappend (Expected [] _ _ _) r = r
        mappend l (Expected [] _ _ _) = l
        mappend p@(Expected e b c l) p'@(Expected e' b' c' l') = case compare c c' of
                                                                      LT -> p'
                                                                      GT -> p
                                                                      EQ -> case compare l l' of
                                                                                 LT -> p'
                                                                                 GT -> p
                                                                                 EQ -> p
-}
