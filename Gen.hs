module Gen (generate) where

import qualified Ast as A

genE :: A.Expr -> IO ()
genE (A.Bin op l r) = do
        genE l
        putStr "pushq %rax\n\t"
        genE r
        putStr $ case op of
                    A.Add -> build "addq"
                    A.Sub -> build "subq"
                    A.Mul -> build "mulq"
                    A.Div -> "movq %rax, %rbx\n\t" ++ "popq %rax\n\t" ++ "xorq %rdx, %rdx\n\t" ++ "divq %rbx\n\t"
        where build op = "popq %rbx\n\t" ++ op ++ " %rbx, %rax\n\t"
genE (A.Const num) = putStr $ "movq $" ++ show num ++ ", %rax\n\t"

generate :: A.Expr -> IO ()
generate e =  do
        putStr $ ".text\n\t" ++ ".global _start\n" ++ "_start:\n\t"
        genE e
        putStr "movq %rax, %rdi\n\t"
        putStr "call printi\n\t"
        putStr "movq $60, %rax\n\t"
        putStr "movq $0, %rdi\n\t"
        putStr "syscall\n"
