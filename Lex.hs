module Lex (tokenize, Token(..), TokPos) where

import Data.Maybe (catMaybes, listToMaybe, mapMaybe)
import Data.Char (isDigit, digitToInt, isAsciiLower)
import Data.List (stripPrefix)

data Token = Error Char
           {- Keywords -}
           | KwInt
           | KwFloat
           {- Identifier -}
           | Ident String
           {- Constant -}
           | Const Int
           {- String-literal -}
           | Lit String
           {- Punctuators -}
           | Add
           | AddAdd
           | AddAssign
           | Sub
           | SubSub
           | SubAssign
           | Mul
           | MulAssign
           | Div
           | DivAssign
           | Semi
           | Comma
           | Assign
           | AssignAdd
           | Lpar
           | Rpar
           | Lcur
           | Rcur
           | Lsqr
           | Rsqr
           | Eof
           deriving (Show, Eq)

--type Pos = (Int, Int)
type TokPos = (Token, Int, Int)

tokenize :: String -> [TokPos]
tokenize = go 0 0
        where
                go c l [] = [(Eof, c, l)]
                go c l (' ':xs) = go c (l + 1) xs
                go c _ ('\n':xs) = go (c + 1) 0 xs
                --go c l xs = foldr (\f acc -> f xs) [] [mbPunc, mbKeyword, mbConst, mbIdent]
                go c l xs = case listToMaybe . catMaybes . fmap ($ xs) $
                        [mbPunc, mbKeyword, mbConst, mbIdent] of
                        Just (xs, sz, tok) -> (tok, c, l) : go c (l + sz) xs
                        Nothing -> (Error $ head xs, c, l) : []
{-
data Trie a b = Trie a b [Trie a b]

punctuators :: Trie Char Token
punctuators = Trie '+' Add [ Trie '=' AddAssign []
                           , Trie '+' AddAdd []
                           ]
-}
{-
punctuators :: [(String, Token)]
punctuators = [ ("+", Add)
              , ("+=", AddAssign)
              , ("++", AddAdd)
              ]
-}

--data Lexer = Lexer (String -> (String, Int, Token))

{- Maybe: use ST as Lexer -}

--punc :: String -> (String, Int, Token)

{-
mb :: [String -> Maybe (String, Int, Token)]
mb = [ mbPunc, mbKeyword, mbConst, mbIdent ]
-}

mbPunc :: String -> Maybe (String, Int, Token)
mbPunc ('+':xss) = case xss of ('=':xs) -> Just (xs, 2, AddAssign)
                               ('+':xs) -> Just (xs, 2, AddAdd)
                               _ -> Just (xss, 1, Add)
mbPunc ('-':x) = case x of ('=':n) -> Just (n, 2, SubAssign)
                           ('-':n) -> Just (n, 2, SubSub)
                           _ -> Just (x, 1, Sub)
mbPunc ('*':x) = case x of ('=':n) -> Just (n, 2, MulAssign)
                           _ -> Just (x, 1, Mul)
mbPunc ('/':x) = case x of ('=':n) -> Just (n, 2, DivAssign)
                           _ -> Just (x, 1, Div)
mbPunc ('(':x) = Just (x, 1, Lpar)
mbPunc (')':x) = Just (x, 1, Rpar)
mbPunc ('{':x) = Just (x, 1, Lcur)
mbPunc ('}':x) = Just (x, 1, Rcur)
mbPunc (';':x) = Just (x, 1, Semi)
mbPunc (',':x) = Just (x, 1, Comma)
mbPunc ('=':x) = Just (x, 1, Assign)
mbPunc _ = Nothing

keywords :: [((Token, String), Int)]
keywords = fmap (\x -> (x, length $ snd x))
        [ (KwInt, "int")
        , (KwFloat, "float")
        ]

-- FIXME: returned valid keyword even next char isn't space, example: "intf" return correct int type
mbKeyword :: String -> Maybe (String, Int, Token)
mbKeyword xs = listToMaybe . catMaybes $ fmap f keywords
        where f (x, l) | Just xs' <- stripPrefix (snd x) xs = Just (xs', l, fst x)
                       | otherwise = Nothing

mbConst :: String -> Maybe (String, Int, Token)
mbConst xs = if null l then Nothing else Just (r, length l, Const $ foldl f 0 l)
        where (l, r) = span isDigit xs
              f n x = n * 10 + digitToInt x

mbIdent :: String -> Maybe (String, Int, Token)
mbIdent xs = case span isAsciiLower xs of
                   ([], _) -> Nothing
                   (l, r) -> Just (r, length l, Ident l)
